# Set $PATH
export EDITOR="nvim"
export PATH=$PATH:~/scripts
export VIDEOS="~/vids"
export BROWSER="firefox"
export READER="zathura"
export TERM="alacritty"
export WM="bspwm"
# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored _correct _approximate
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list ''
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle :compinstall filename '/home/wheatgrass/.zshrc'

autoload -Uz compinit
compinit

# End of lines added by compinstall
# Lines configured by zsh-newuser-install

HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=20000
setopt autocd extendedglob nomatch
unsetopt beep

# Load version control information
autoload -Uz vcs_info
precmd() { vcs_info }

# Format the vcs_info_msg_0_ variable
zstyle ':vcs_info:git:*' formats 'on branch %b'
 
# Set up the prompt (with git branch name)
setopt PROMPT_SUBST
#PROMPT='%n in ${PWD/#$HOME/~} ${vcs_info_msg_0_} > '

# Prompt
export PS1="[%n@%M %~] ${vcs_info_msg_0_} %% "

# Alias'
alias mv='mv -v'
alias cp='cp -v'
alias rm='rm -v'
alias ls='ls --color'
alias killall='killall -v'
alias ttymatrix='unimatrix -n -s 96 -l o'
alias v='nvim'
alias ls='lsd'


# Vim Mode
bindkey -v

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
